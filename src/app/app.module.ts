import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { KonamiModule } from 'ngx-konami';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LookupFormComponent } from './lookup-form/lookup-form.component';
import { StateJsonPreviewComponent } from './state-json-preview/state-json-preview.component';
import { ResultLinkComponent } from './result-link/result-link.component';

@NgModule({
  declarations: [
    AppComponent,
    LookupFormComponent,
    StateJsonPreviewComponent,
    ResultLinkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    KonamiModule,
    environment.production ? [] : [AkitaNgDevtools.forRoot(), AkitaNgRouterStoreModule.forRoot()]
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
