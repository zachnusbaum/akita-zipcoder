import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface RootState {
  zip: string;
  city: string;
  state: string;
  latitude: string;
  longitude: string;
}

export function createInitialState(): RootState {
  return {
    zip: null,
    city: null,
    state: null,
    latitude: null,
    longitude: null
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'Root' })
export class RootStore extends Store<RootState> {

  constructor() {
    super(createInitialState());
  }

}

