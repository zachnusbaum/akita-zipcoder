import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { RootStore } from './root.store';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class RootService {

  constructor(private rootStore: RootStore,
              private http: HttpClient) {
  }

  public setZipcode(zipcode: string) {
    this.rootStore.update({zip: zipcode});
    this.geocode(zipcode);
  }

  public resetStore() {
    this.rootStore.reset();
  }

  public geocode(zipcode: string) {
    this.http.get(`https://petshop.zchry.cc/api/zip/${zipcode}`).subscribe((response: any) => {
      this.rootStore.update({
        city: response.result.city,
        state: response.result.state,
        latitude: response.result.latitude,
        longitude: response.result.longitude
      });
    }, (error: any) => {
      this.rootStore.update({
        latitude: null,
        longitude: null,
        city: null,
        state: null
      });
    });
  }

  get() {
    // this.http.get().subscribe((entities: ServerResponse) => {
      // this.rootStore.set(entities);
    // });
  }

  add() {
    // this.http.post().subscribe((entity: ServerResponse) => {
      // this.rootStore.add(entity);
    // });
  }

}
