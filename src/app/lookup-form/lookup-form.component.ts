import { RootService } from './../state/root.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lookup-form',
  templateUrl: './lookup-form.component.html',
  styleUrls: ['./lookup-form.component.scss']
})
export class LookupFormComponent implements OnInit {

  constructor(private rootService: RootService) { }

  ngOnInit() {
  }

  onSubmit(zipcode: string) {
    this.rootService.setZipcode(zipcode);
  }

}
