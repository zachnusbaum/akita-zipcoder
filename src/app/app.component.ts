import { RootQuery } from './state/root.query';
import { Component, OnInit } from '@angular/core';
import { RootService } from './state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ZipAkita';
  constructor(private rootService: RootService, private rootQuery: RootQuery) {}
  state$ = this.rootQuery.select(state => state);
  ngOnInit() {
    // this.rootService.setZipcode('48025');
    console.log(this.rootQuery.getSnapshot());
  }

  konami() {
    alert('Party time!');
    this.rootService.resetStore();
  }
}
