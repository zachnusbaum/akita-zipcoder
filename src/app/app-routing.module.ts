import { ResultLinkComponent } from './result-link/result-link.component';
import { StateJsonPreviewComponent } from './state-json-preview/state-json-preview.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: StateJsonPreviewComponent },
  { path: 'link', component: ResultLinkComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
