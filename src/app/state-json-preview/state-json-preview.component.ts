import { Component, OnInit } from '@angular/core';
import { RootStore } from '../state/root.store';
import { RootQuery } from '../state/root.query';
import { DirtyCheckPlugin } from '@datorama/akita';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-state-json-preview',
  templateUrl: './state-json-preview.component.html',
  styleUrls: ['./state-json-preview.component.scss']
})
export class StateJsonPreviewComponent implements OnInit {
  state$ = this.rootQuery.select(state => state);
  zipPop$;

  dirtyCheck: DirtyCheckPlugin;

  constructor(public rootStore: RootStore, private rootQuery: RootQuery) { }

  ngOnInit() {
    this.dirtyCheck = new DirtyCheckPlugin(this.rootQuery, {watchProperty: 'zip'}).setHead();
    this.zipPop$ = this.state$.pipe(map(state => {
      if (state.zip === null) { return false; }
      return true;
    }));
    console.log(this.dirtyCheck);
  }

}
