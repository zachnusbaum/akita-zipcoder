import { DirtyCheckPlugin } from '@datorama/akita';
import { RootQuery } from './../state/root.query';
import { Component, OnInit } from '@angular/core';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'app-result-link',
  templateUrl: './result-link.component.html',
  styleUrls: ['./result-link.component.scss']
})
export class ResultLinkComponent implements OnInit {
  zipcode$ = this.rootQuery.select(state => state.zip);
  zipPop$;
  latLon$ = this.rootQuery.select(state => `${state.latitude},${state.longitude}`);

  dirtyCheck: DirtyCheckPlugin;

  constructor(private rootQuery: RootQuery) {
    this.dirtyCheck = new DirtyCheckPlugin(this.rootQuery, {watchProperty: 'zip'});
    if (!this.dirtyCheck.hasHead()) { this.dirtyCheck.setHead(); }
    this.zipPop$ = this.zipcode$.pipe(map(zipcode => {
      if (zipcode === null) { return false; }
      return true;
    }));
  }

  ngOnInit() {
  }

}
